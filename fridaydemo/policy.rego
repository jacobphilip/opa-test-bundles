package envoy.http.roles

default allow = false

allow {

	admin_users[claims.username]
}

admin_users := {
	"vivek",
	"sandeep",
	"jacob"
}

claims := payload {
	v := input.attributes.request.http.headers.authorization
	startswith(v, "Bearer ")
	t := substring(v, count("Bearer "), -1)
	io.jwt.verify_hs256(t, "B41BD5F462719C6D6118E673A2389")
	[_, payload, _] := io.jwt.decode(t)
}

